import logging
import pathlib
import re
import warnings

import pandas as pd

from keboola.component.base import ComponentBase
from keboola.component.exceptions import UserException

# configuration variables
KEY_ADD_FILE_NAME = 'addFileName'
KEY_SELECT_SHEETS = 'selectSheets'
KEY_IGNORE_SHEETS = 'ignoreSheets'

MANDATORY_PARS = []

SUPPORTED_FORMATS = [{"file_format": "xlsx", "engine": "openpyxl"},
                     {"file_format": "xls", "engine": None},
                     {"file_format": "xlsb", "engine": "pyxlsb"}]


class Component(ComponentBase):

    def __init__(self):
        self.addFileName = None
        self.selectSheets = None
        self.ignoreSheets = None
        super().__init__()

    def run(self):
        self.validate_configuration_parameters(MANDATORY_PARS)

        # Get proper list of tables
        in_files = self.get_input_files_definitions()
        logging.info(f"IN files mapped: {[file.full_name for file in in_files]}")

        params = self.configuration.parameters
        self.addFileName = params.get(KEY_ADD_FILE_NAME)
        self.selectSheets = params.get(KEY_SELECT_SHEETS)
        self.ignoreSheets = params.get(KEY_IGNORE_SHEETS)

        for file in in_files:
            self._process_xlsx(file)

        logging.info("processor-xlsx2csv finished")

    def _process_xlsx(self, in_file):

        logging.info(f'Processing {in_file.full_name}...')

        xls_input = self._open_as_unknown_format(in_file)

        if not xls_input:
            raise UserException(f'Error reading sheets from file: {in_file.full_name}')

        xls_sheets = xls_input.sheet_names
        logging.info(f'Available sheets: {xls_sheets}')
        sheets_to_output = self._filter_sheets(xls_sheets)

        # Output table name adjustment
        output_table_name = self._clean_name(in_file)

        # Output all selected sheets
        for sheet in sheets_to_output:
            sheet_name = re.sub(r'\s|-', '_', re.sub(r'\(|\)', '', sheet))
            data = xls_input.parse(sheet)
            output_file = self.create_out_file_definition(f"{output_table_name}{sheet_name}.csv")
            logging.info(f'Exporting {output_file.full_name}')
            data.to_csv(output_file.full_path, index=False)

        logging.info(f'Processing {in_file.full_name} finished')

    @staticmethod
    def _log_user_warning(message, category, filename, lineno, file=None, line=None):
        logging.info(f'{filename}:{lineno}: {category.__name__}: {message}')

    def _open_as_unknown_format(self, in_file):
        for supported_format in self._format_estimation(in_file):
            logging.info(f'Trying to read file {in_file.full_name} as format: {supported_format["file_format"]}')
            try:
                with warnings.catch_warnings():
                    warnings.showwarning = self._log_user_warning
                    return pd.ExcelFile(in_file.full_path, engine=supported_format['engine'])
            except Exception as e:
                logging.info(
                    f'Error reading file {in_file.full_name} as format:'
                    f' {supported_format['file_format']} with error: {e}')
                continue

    def _format_estimation(self, in_file):
        extension = self._get_extension(in_file.full_path)
        supported_formats = []
        if extension != '':
            logging.info(f'File extension: {extension}')
            extension = extension.lower()
            supported_formats = [supported_format for supported_format in SUPPORTED_FORMATS if
                                 supported_format['file_format'] == extension]
        if not supported_formats:
            logging.info(f'File extension: "{extension}" is not known, trying all supported formats')
            supported_formats = SUPPORTED_FORMATS
        return supported_formats

    @staticmethod
    def _get_extension(file):
        return pathlib.Path(file).suffix.replace('.', '')

    def _clean_name(self, in_file):
        output_table_name = ''
        if self.addFileName:
            output_table_name = in_file.full_name
            output_table_name = self._replace_supported_formats_extensions(output_table_name)
            output_table_name = output_table_name.strip()
            output_table_name = output_table_name.replace(' ', '_')
            output_table_name = output_table_name + '_'
        return output_table_name

    def _replace_supported_formats_extensions(self, output_table_name):
        extension = self._get_extension(output_table_name)
        return output_table_name.replace(f'.{extension}', '')

    def _filter_sheets(self, input_sheets):
        """
        Filtering sheets requested
        """

        if self.selectSheets:
            logging.info(f'Sheets to select: {self.selectSheets}')
            selected_sheets = []
            for sheet in self.selectSheets:
                if sheet in input_sheets:
                    selected_sheets.append(sheet)
                else:
                    raise UserException(f'{sheet} is not found. Please validate input "selectSheets"')
            return selected_sheets

        elif self.ignoreSheets:
            logging.info(f'Sheets to ignore: {self.ignoreSheets}')
            selected_sheets = input_sheets
            for sheet in self.ignoreSheets:
                if sheet in input_sheets:
                    selected_sheets.remove(sheet)
                else:
                    raise UserException(f'{sheet} is not found. Please validate input "ignoreSheets"')
            return selected_sheets

        else:
            return input_sheets


"""
        Main entrypoint
"""
if __name__ == "__main__":
    try:
        comp = Component()
        # this triggers the run method by default and is controlled by the configuration.action parameter
        comp.execute_action()
    except UserException as exc:
        logging.exception(exc)
        exit(1)
    except Exception as exc:
        logging.exception(exc)
        exit(2)
